#include <stdlib.h>
#include <gtk/gtk.h>

static void helloWorld (GtkWidget *wid, GtkWidget *win)
{
    GtkWidget *dialog = NULL;

    dialog = gtk_message_dialog_new (GTK_WINDOW (win), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_CLOSE, "Hello World!");
    gtk_window_set_position (GTK_WINDOW (dialog), GTK_WIN_POS_CENTER);
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
}

int main (int argc, char *argv[])
{
    GtkWidget *button[500];
    GtkWidget *win[500];
    GtkWidget *vbox[500];

    for(int i=0;i<=500;i++){
        win[i]=NULL;
        vbox[i]=NULL;
        button[i]=NULL;
    }

    g_log_set_handler ("Gtk", G_LOG_LEVEL_WARNING, (GLogFunc) gtk_false, NULL);
    gtk_init (&argc, &argv);
    g_log_set_handler ("Gtk", G_LOG_LEVEL_WARNING, g_log_default_handler, NULL);


    for(int i=0;i<=500;i++){

        win[i] = gtk_window_new (GTK_WINDOW_TOPLEVEL);
            gtk_container_set_border_width (GTK_CONTAINER (win[i]), 8);
            gtk_window_set_title (GTK_WINDOW (win[i]), "Hello World");
            gtk_window_set_position (GTK_WINDOW (win[i]), GTK_WIN_POS_CENTER);
            gtk_widget_realize (win[i]);
            g_signal_connect (win[i], "destroy", gtk_main_quit, NULL);

        vbox[i] = gtk_vbox_new (TRUE, 6);
        gtk_container_add (GTK_CONTAINER (win[i]), vbox[i]);

        button[i] = gtk_button_new_from_stock (GTK_STOCK_DIALOG_INFO);
            g_signal_connect (G_OBJECT (button[i]), "clicked", G_CALLBACK (helloWorld)
            ,(gpointer) win[i]);
            gtk_box_pack_start (GTK_BOX (vbox[i]), button[i], TRUE, TRUE, 0);

        button[i] = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
            g_signal_connect (button[i], "clicked", gtk_main_quit, NULL);
            gtk_box_pack_start (GTK_BOX (vbox[i]), button[i], TRUE, TRUE, 0);

        gtk_widget_show_all (win[i]);
    }

    gtk_main ();
    return 0;
}
