### Note 

- This respostory is **GPL3** all code and document but, some changes for docs;Documents can be distributed provided that the source is specified. 
- Örnekler benim tarafımdan sıfırdan yazılmış olup **GPL3** lisansındadır.Değişiklikler: Dökümanları görünebilecek bir yere kaynak belirmeniz şartı ile istediğiniz gibi kullanabilirsiniz.

# Contents  <QueryVS C&CPP Handbook> 
- [Compile from terminal GCC(GNU Compile Collection) using](#compile)
    - [Dependencies](#depen)
    - [Some Commands](#somc)
    
- [C-CPP-examples](#ccppex)
    - [General information of programing language](#genin)
        - [Sources](#sources)
        - [The directories to know](#direct)
        - [the Text editor I use](#teditor)
        - [Build generates](#buildgen)
    - [C Programing Languagage Wordlist](#cplwordlist)
        - [Operators](#operators)
        - [Conditions](#condititions)
        - [Variables and Dimensions](#variables)
        - [Other Wordlist](#otherwordlist)
        - [Preprocessors](#preprocessing)
            - [Expressions](#expressions)
            - [Create Header](#createheader)
        - [Pointer](#pointer)
        - [Linked List](#linkedlist)
    - [C standards](#csdrs)
    - [Code Styles](#codestyle)
    - [C Standard Libraries](#csl)
- [Sources](#sources)

<a name="compile"/>

# Compile from terminal GCC(GNU Compile Collection) using 

<a name="depen"/>

## Dependencies 

```
gentoo ~ $ ldd $(which gcc)

```

```
linux-vdso.so.1 (0x00007ffca6fb9000)
    libm.so.6 => /lib64/libm.so.6 (0x00007fa761584000)
    libc.so.6 => /lib64/libc.so.6 (0x00007fa7613c6000)
    /lib64/ld-linux-x86-64.so.2 (0x00007fa7616d2000)
```
<a name="somc"/>

## Some Commands

### Just Compile(Dynamic)
```
gentoo ~ $ gcc source.c -o compiledbinaryfile 
gentoo ~ $ gcc -o compiledbinaryfile source.c
```


#### Some Parameters
```
-Wall :Print all warning messages
-Werror :Print all error messages
-std=standard :Write standard and compile the standard
-static :Compile static
-llibname(just if inside /lib /usr/lib /lib64.) :add dynamic or static lib
-Lpath/libname(writable path your lib) :add dynamic or static lib
```
#### Compile GTK+-3.0 Application from terminal

```
gentoo ~ $ gcc -Wall -g main.c -o main \
`pkg-config --cflags gtk+-3.0` \
`pkg-config --libs gtk+-3.0`  # Okay 

```
<a name="ccppex"/>
# C-CPP-examples

- socket
- math problems
- game
- encryption
- database
- console programs 


with c and c++. Examples projects written by query. Licence GPL3+ but just if you are using, specify source.


<a name="genin"/>

## General information of programing language

Compilable programs consist of binary and source. the C and C++ is compilable a programing language. My compiler is GCC(GNU compiler collection). My Operating System is GNU/Linux now [Gentoo](https://www.gentoo.org/)

<a name="sources"/>

### Sources
- [CPPReference](https://en.cppreference.com/w/c)
- [Keywords](https://en.cppreference.com/w/c/keyword)
- [Storage Duration](https://en.cppreference.com/w/c/language/storage_duration)
- [C Programing wiki page](https://en.wikipedia.org/wiki/C_(programming_language))

<a name="direct"/>

### The directories to know
- /lib
- /lib64
- /lib32
- /usr/share/lib
- /usr/lib
- /usr/lib64
- /usr/lib32
- /usr/include

<a name="compile"/>

### the Text editor I use
- nano
- vim
- vi

<a name="compile"/>

### Build generates
- GNU autotools
- cmake

<a name="cplwordlist"/>

## C Programing Languagage Wordlist

<a name="operators"/>

#### Operators 
1. **Conditional**: ==,!=,<=,>=,<,>
1. **Logical**: ||,&&,!
1. **Mathematical**: +,-,*,/,%,++,--
1. **Assignment**: =,+=,-=,*=,/=,%=,>>=,<<=,&=,|=,^=
1. **Bitwise**: &,|,~,^,<<,>>

<a name="condititions"/>

#### Conditions
```
if(){

}else if(){

}else{
    
}
``` 
**Ternary Operator**
```
conditions ? if : else;
a<b ? a=1 : b=1;
```

```
switch(expression){
    case ..:
        ...
        break;
    case ..:
        ...
        break;
    default:
        ...
}
```

<a name="variables"/>

#### Variables and Dimensions

- char : 1byte    :-128 to 127 or 0 to 255
- unsigned char:1byte    :0 to 255
- signed char: 1byte    :-128 to 127
- int: 2 or 4 bytes    :-32,768 to 32,767 or -2,147,483,648 to 2,147,483,647
- unsigned int: 2 or 4 bytes    :0 to 65,535 or 0 to 4,294,967,295
- short: 2bytes    :-32,768 to 32,767
- unsigned short: 2bytes    :0 to 65,535
- long: 8bytes or (4 bytes for 32 bit OS)    :-9223372036854775808 to 9223372036854775807
- unsigned long:8bytes    :0 to 18446744073709551615
- 
- float:4bytes    :1.2E-38 to 3.4E+38
- double:8bytes    :2.3E-308 to 1.7E+308
- long double:10 bytes    :3.4E-4932 to 1.1E+4932

<a name="otherwordlist"/>

#### Other Wordlist
```
typedef unsigned int u_int;
//using unsigned int u_int
//u_int number;
```
```
struct datatypename{} firstvariable;
//or 
struct datatypename{};
//using 
struct datatypename firstvariable;
//or if don`t want write struct,use typedef 
typedef struct datatypename{} firstvariable;
```

<a name="preprocessing"/>

### Preprocessors
- Preprocessors work before compile and arrange code. It is useful for writing cross-platform code and any situation where code needs to be edited before compiling.

<a name="expressions"/>

##### Expressions
```
#if expression1
...block1
#elif expressions2
...block2
#else
...block3
#endif
```
- if expression1=true, active block1 and adding compiling stage. if expression1=false expression2=true,active block2 and adding compiling stage and if expression1,expression2=false, active block3.

```
#define variable 11
#undefine variable

#ifdef variable
...
#endif

#ifndef variable
...
#endif
```

```
#warning "show warning on compiling"
#message "normal message on compiling"
#pragma
#error "error message on compiling exiting compiling"
```

<a name="createheader"/>

##### Create Header

- .h files is header files.

project folder tree
    |---main.h
    |---main.c

main.h  -->
```
#ifndef MAİN_H
#define MAİN_H
#include....
int ...
struct .... //about
func();
int func2();
//your function,header and variable definition
#endif //MAIN_H
```
main.c --> 
```
#include "main.h"
main(){
    func2;
}
```

<a name="pointer"/>

### Pointer

- Necessary for data structure, and learn lvalue and rvalue. 

```
int *a,b=10;
        a = &b;
        printf("*a=%d,a=%p,&a=%p", *a,a,&a);
        printf("\nb=%d,&b=%p\n",b,&b);
```

<a name="linkedlist"/>

### Linked List

```
#include<stdio.h>
#include<stdlib.h>
typedef struct linked{
        int a;
        struct linked *next;
}linked_;


void main(void){
        linked_ *head = NULL;
        head = (linked_ *) malloc(sizeof(linked_));

        head->a=1;
        head->next = (linked_ *) malloc(sizeof(linked_));
        head->next->a = 2;
        head->next->next = NULL;
        printf("%d",head->a);
}
```

<a name="compile"/>

## C standards
C or C++ standards about changed and added new features or removed or changed features on the C language.

- [History of C](https://en.cppreference.com/w/c/language/history)
- [History of C++](https://en.cppreference.com/w/cpp/language/history)

<a name="codestyle"/>

## Code Styles 

-Code writing standards exist to increase readability and writability.This like;

```
while (x == y)
{
    something();
    somethingelse();
}//Allman

while (x == y) {
    something();
    somethingelse();
}//K&R

while (x == y)
  {
    something ();
    somethingelse ();
  }//GNU

while (x == y)
    {
    something();
    somethingelse();
    }//Whitesmiths

while (x == y)
{   something();
    somethingelse();
}//Horstmann

while (x == y)
  { something()
  ; somethingelse()
  ;
  }//Haskell

while (x == y)
{   something();
    somethingelse(); }//Pico

while (x == y) {
    something();
    somethingelse();
    }//Ratliff

while (x == y)
  { something();
    somethingelse(); }//Lisp

```

<a name="csl"/>

## C Standard Libraries

[Wiki Page](https://en.wikipedia.org/wiki/C_standard_library)

<a name="sources">
## Sources
[GTK compile from terminal](https://developer.gnome.org/gtk-tutorial/stable/x111.html)

## okay guys this much. You'd better research these.
# goodby
